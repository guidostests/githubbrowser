package com.guidovezzoni.githubbrowser;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guido on 15/05/2015.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    ListView mListView = null;
    EditText mRepoOwner = null;
    EditText mRepoName = null;
    Button mButton = null;


    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);

        // gets the UI elements
        final MainActivity activity = getActivity();
        mListView = (ListView) activity.findViewById(R.id.commits_list_view);
        mButton = (Button) activity.findViewById(R.id.button);
        mRepoOwner = (EditText) activity.findViewById(R.id.repo_owner_edit_text);
        mRepoName = (EditText) activity.findViewById(R.id.repo_name_edit_text);


        // populate the lv with test data to allow a repeatable test
        final List<GitHubAPIClient.Item> items = new ArrayList<GitHubAPIClient.Item>();

        for (int ii = 0; ii < 50; ii++)
            items.add(new GitHubAPIClient.Item("sha",
                    new GitHubAPIClient.Commit(new GitHubAPIClient.Author("Name", "date")),
                    "http:://"));

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.setAdapater(items);
            }
        });
    }


    public void testListCount() throws InterruptedException {
        getInstrumentation().waitForIdleSync();
        assertEquals(50, mListView.getAdapter().getCount());
    }

    public void testTouchEvents() {
        TouchUtils.scrollToBottom(this, getActivity(), mListView);
        getInstrumentation().waitForIdleSync();
        assertEquals(49, mListView.getLastVisiblePosition());
    }

}
